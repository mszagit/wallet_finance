<?php

namespace User\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserLoginBundle:Default:index.html.twig');
    }
    
    public function registerAction(){
        return $this->render('UserLoginBundle:Default:register.html.twig');
    }
}
